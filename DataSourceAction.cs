/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Datacollector.Sources;
using Frank.Actions;
using Datacollector.Configuration;

namespace Datacollector.Ui
{
    /// <summary>
    /// Action for data sources.
    /// </summary>
    public class DataSourceAction<T>: DataAttachedAction<DataSource<T>>
        where T: DataSourceSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.DataSourceAction`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dataSource">Data source.</param>
        /// <param name="actionName">Action name.</param>
        public DataSourceAction (I18n i18n, DataSource<T> dataSource, string actionName):
            base(dataSource, actionName, TranslateForConstructor(i18n, Constants.UiDomain, "{0}", dataSource.Name))
        {
        }

        private static TranslationString TranslateForConstructor(I18n i18n, string domain, string format, params object[] arguments)
        {
            Initializer.Initialize(i18n);
            return i18n.TrObject (domain, format, arguments);
        }
    }
}

