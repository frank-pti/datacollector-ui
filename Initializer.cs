/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace Datacollector.Ui
{
    /// <summary>
    /// Initializer for internationalization.
    /// </summary>
    public static class Initializer
    {
        private static bool initialized = false;

        /// <summary>
        /// Initialize the specified i18n.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public static void Initialize (I18n i18n)
        {
            if (!initialized) {
                LoadLanguage(i18n, i18n.Language);
                i18n.LanguageChanged += LoadLanguage;
                initialized = true;
            }
        }

        private static void LoadLanguage(I18n i18n, string language)
        {
            i18n.LoadFromResource(Constants.UiDomain, String.Format(
                "{0}.{1}.json", Constants.I18nResourcePrefix, language));
        }
    }
}
