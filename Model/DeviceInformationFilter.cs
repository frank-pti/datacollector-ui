/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Frank.Widgets.Model;
using ProbeNet.Backend;

namespace Datacollector.Ui.Model
{
    /// <summary>
    /// Device information filter.
    /// </summary>
    public class DeviceInformationFilter: GenericTreeModelFilter<DeviceInformation, string, Gdk.Pixbuf>
    {
        /// <summary>Delegate method for device visible function.</summary>
        public delegate bool DeviceVisibleFunc(IDeviceInformationStore deviceStore, Gtk.TreeIter iter);

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Model.DeviceInformationFilter"/> class.
        /// </summary>
        /// <param name="store">Store.</param>
        /// <param name="path">Path.</param>
        public DeviceInformationFilter (IDeviceInformationStore store, Gtk.TreePath path):
            base(store, path)
        {
            base.VisibleFunc = new TreeModelFilterVisibleFunc<DeviceInformation, string, Gdk.Pixbuf>(FilterTree);
        }

        private bool FilterTree (IGenericTreeModel<DeviceInformation, string, Gdk.Pixbuf> model, Gtk.TreeIter iter)
        {
            if (VisibleFunc == null) {
                return true;
            }
            return VisibleFunc(model as IDeviceInformationStore, iter);
        }

        /// <summary>
        /// Gets or sets the visible func, which defines whether the given tree path should be visible.
        /// </summary>
        /// <value>The visible func.</value>
        public new DeviceVisibleFunc VisibleFunc {
            set;
            get;
        }
    }
}

