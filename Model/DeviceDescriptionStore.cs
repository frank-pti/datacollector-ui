using Frank.Widgets.Model;
using Internationalization;
using ProbeNet.Messages.Raw;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Model
{
    /// <summary>
    /// List store for storing device descriptions.
    /// <list type="bullet">
    /// <item><description>Guid</description><value>The UUID of the data source</value></item>
    /// <item><description>Dictionary[string, DeviceDescription]</description><value>The device descriptions for each installed languages</value></item>
    /// <item><description>string</description><value>The device caption</value></item>
    /// <item><description>string</description><value>The serial number</value></item>
    /// <item><description>string</description><value>The IP address</value></item>
    /// <item><description>int</description><value>The port</value></item>
    /// <item><description>Gdk.Pixbuf</description><value>The device icon</value></item>
    /// <item><description>bool</description><value>Sensitive</value></item>
    /// </list>
    /// </summary>
    public class DeviceDescriptionStore : GenericListStore<Guid, Dictionary<string, DeviceDescription>, string, string, string, int, Gdk.Pixbuf, bool, TranslationString>
    {
        internal TranslationString WarningConfiguredText;
        internal TranslationString WarningCachedText;

        public DeviceDescriptionStore() :
            this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Model.DeviceDescriptionStore"/> class.
        /// </summary>
        public DeviceDescriptionStore(I18n i18n)
        {
            if (i18n != null) {
                Initialize(i18n);
            }
        }

        public void Initialize(I18n i18n)
        {
            WarningConfiguredText = i18n.TrObject(Constants.UiDomain, "Datasource already configured");
            WarningCachedText = i18n.TrObject(Constants.UiDomain, "Information cached");
        }

        /// <summary>
        /// Finds the device by its Guid.
        /// </summary>
        /// <returns>The device.</returns>
        /// <param name="guid">GUID.</param>
        public int FindDevice(Guid guid)
        {
            int i = 0;
            foreach (var entry in this) {
                if (entry.Item1 == guid) {
                    return i;
                }
                i++;
            }
            return -1;
        }
    }
}

