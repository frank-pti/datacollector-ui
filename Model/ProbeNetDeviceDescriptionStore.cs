using Internationalization;
using ProbeNet.DeviceDiscovery;
using ProbeNet.Messages.Raw;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace Datacollector.Ui.Model
{
    /// <summary>
    /// ProbeNet device description store.
    /// </summary>
    public class ProbeNetDeviceDescriptionStore : DeviceDescriptionStore
    {
        private DeviceObserver observer;
        private string language;
        private IEnumerable<Guid> configuredDataSources;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Model.ProbeNetDeviceDescriptionStore"/> class.
        /// </summary>
        /// <param name="observer">Observer.</param>
        public ProbeNetDeviceDescriptionStore(I18n i18n, DeviceObserver observer, string language, IEnumerable<Guid> configuredDataSources) :
            base(i18n)
        {
            this.observer = observer;
            this.language = language;
            this.configuredDataSources = configuredDataSources;
            observer.DeviceAdded += HandleDeviceAdded;
            observer.DeviceRemoved += HandleDeviceRemoved;
            LoadDevicesFromWatcher(observer);
        }

        private void LoadDevicesFromWatcher(DeviceObserver observer)
        {
            foreach (Guid guid in observer.Devices.Keys) {
                Tuple<string, int> address = observer.Addresses[guid];
                HandleDeviceAdded(guid, address.Item1, address.Item2);
            }
        }

        private void HandleDeviceAdded(Guid guid, string address, int port)
        {
            Dictionary<string, DeviceDescription> descriptions = observer.Devices[guid];
            DeviceDescription description = null;
            if (descriptions.ContainsKey(language)) {
                description = descriptions[language];
            } else {
                description = descriptions.First().Value;
            }
            Gdk.Pixbuf icon = null;
            if (description.HasIcon) {
                icon = new Gdk.Pixbuf(description.Icon);
            }
            bool configured = configuredDataSources.Contains(description.Uuid);
            Gtk.Application.Invoke((o, e) =>
                base.Add(guid, descriptions, description.Caption, description.SerialNumber, address, port, icon,
                !configured, configured ? base.WarningConfiguredText : null));
        }

        private void HandleDeviceRemoved(Guid guid)
        {
            int position = base.FindDevice(guid);
            if (position >= 0) {
                Gtk.Application.Invoke((o, e) => RemoveAt(position));
            }
        }
    }
}

