/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Frank.Widgets.Model;
using System.Collections.Generic;
using ProbeNet.Backend;
using Internationalization;
using Datacollector.Sources;
using Frank.Widgets;

namespace Datacollector.Ui.Model
{
    /// <summary>
    /// Class for loading device information list.
    /// </summary>
    public class DeviceInformationListLoader
    {
        /// <summary>
        /// Loads the device store.
        /// </summary>
        /// <returns>The device store.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="manufacturerList">Manufacturer list.</param>
        public static DeviceInformationStore LoadDeviceStore(I18n i18n, out IList<ManufacturerInformation> manufacturerList)
        {
            Initializer.Initialize(i18n);
            manufacturerList = new List<ManufacturerInformation>();
            DeviceInformationStore store = new DeviceInformationStore();
            BackendLoader loader = new BackendLoader (i18n);
            foreach (DeviceInformation deviceInformation in loader.LoadBackends()) {
                if (!deviceInformation.Hidden) {
                    store.AppendValues (deviceInformation, deviceInformation.Name.Tr, IconLoader.BuildIcon (deviceInformation.Icon));
                    if (!manufacturerList.Contains(deviceInformation.Manufacturer)) {
                        manufacturerList.Add(deviceInformation.Manufacturer);
                    }
                }
            }
            return store;
        }

        /// <summary>
        /// Loads the manufacturer store.
        /// </summary>
        /// <returns>The manufacturer store.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="manufacturerList">Manufacturer list.</param>
        public static ManufacturerStore LoadManufacturerStore(I18n i18n, IList<ManufacturerInformation> manufacturerList)
        {
            ManufacturerStore store = new ManufacturerStore();
            foreach (ManufacturerInformation manufacturer in manufacturerList) {
                ManufacturerInformation information = manufacturer;
                string unknownString = "Unknown";
                string name = i18n.Tr(Constants.UiDomain, unknownString);
                if (information == null) {
                    information = new ManufacturerInformation(unknownString, null) {
                        IsCatchAll = true
                    };
                } else {
                    name = information.Name;
                }
                store.Add(information, name, IconLoader.BuildIcon(information.Icon));
            }
            ManufacturerInformation containsAll = new ManufacturerInformation("All", null) {
                ContainsAll = true
            };
            store.Add(containsAll, i18n.Tr(Constants.UiDomain, containsAll.Name), null);
            return store;
        }

        /// <summary>
        /// Updates the device store.
        /// </summary>
        /// <param name="deviceStore">Device store.</param>
        /// <param name="source">Source.</param>
        /// <param name="domain">Domain.</param>
        public static void UpdateDeviceStore (DeviceInformationStore deviceStore, I18n source, string domain)
        {
            foreach (GenericListRow<DeviceInformation, string, Gdk.Pixbuf> row in deviceStore) {
                row.Item2 = row.Item1.Name.Tr;
            }
        }

        /// <summary>
        /// Updates the manufacturer store.
        /// </summary>
        /// <param name="manufacturerStore">Manufacturer store.</param>
        /// <param name="source">Source.</param>
        /// <param name="domain">Domain.</param>
        public static void UpdateManufacturerStore (ManufacturerStore manufacturerStore, I18n source, string domain)
        {
            foreach (GenericListRow<ManufacturerInformation, string, Gdk.Pixbuf> row in manufacturerStore) {
                row.Item2 = row.Item1.Name;
            }
        }
    }
}
