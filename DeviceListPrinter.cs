﻿using Datacollector.Ui.Model;
using Gtk;
using Internationalization;
using Logging;
using ProbeNet.Backend;
using System;

namespace Datacollector.Ui
{
    public class DeviceListPrinter
    {
        private I18n i18n;
        private IDeviceInformationStore deviceStore;
        private string programText;

        public DeviceListPrinter(I18n i18n, IDeviceInformationStore deviceStore, string programText)
        {
            this.i18n = i18n;
            this.deviceStore = deviceStore;
            this.programText = programText;
        }

        public void Print()
        {
            PrintOperation print = new PrintOperation();
            print.BeginPrint += BeginPrint;
            print.DrawPage += DrawPage;
            print.EndPrint += EndPrint;
            print.Run(Gtk.PrintOperationAction.PrintDialog, null);
        }

        private void BeginPrint(object o, BeginPrintArgs args)
        {
            int pages = 0;
            int children = deviceStore.IterNChildren();
            if (children > 0) {
                pages = (children - 1) / 25 + 1;
            }
            (o as PrintOperation).NPages = pages;
        }

        private void DrawPage(object o, DrawPageArgs args)
        {
            PrintOperation operation = (PrintOperation)o;
            PrintContext print = args.Context;
            double halfPageWidth = print.Width / 16;
            double fullPageWidth = print.Width / 16 + print.Width / 32;
            Cairo.Context context = print.CairoContext;

            double scale = 1;
            if (Frank.Widgets.Helper.WindowsDetected) {
                scale *= 8.25;
                Logger.Log("Windows detected, manipulating print scale factor to {0}", scale);
            }
            context.Scale(scale, scale);
            double fontSize = 12;
            context.SetFontSize(fontSize);

            context.MoveTo(0, fontSize);
            context.ShowText(i18n.Tr(Constants.UiDomain, "ProbeNet Mini supported devices"));
            context.MoveTo(halfPageWidth, fontSize);
            context.ShowText(i18n.Tr(Constants.UiDomain, "{0}/{1}", args.PageNr + 1, operation.NPages));
            context.MoveTo(fullPageWidth, fontSize);
            string date = String.Format(i18n.DateTimeFormat, "{0}", DateTime.Now);
            context.ShowText(date);
            context.MoveTo(0, fontSize * 1.5);
            Pango.Layout layout = print.CreatePangoLayout();
            layout.SetText(date);
            int width, height;
            layout.GetPixelSize(out width, out height);
            context.RelLineTo(fullPageWidth + width / 8, 0);
            context.Stroke();

            context.Save();
            {
                Cairo.PointD current = context.CurrentPoint;
                Pango.Layout programLayout = print.CreatePangoLayout();
                programLayout.SetText(programText);
                layout.GetPixelSize(out width, out height);
                context.MoveTo(fullPageWidth - 20, fontSize * 1.5 * 2);
                context.ShowText(programText);
                context.MoveTo(current);
            }
            context.Restore();

            int children = deviceStore.IterNChildren();
            double y = context.CurrentPoint.Y + fontSize * 1.5;
            int start = args.PageNr * 25;
            int end = Math.Min(args.PageNr * 25 + 25, children);
            Logger.Log("Print supported devices, page {0} - range: from {1} to {2}", args.PageNr, start, end);
            for (int i = start; i < end; i++) {
                double rowHeight = fontSize * 1.5;
                Gtk.TreeIter deviceIter;
                deviceStore.IterNthChild(out deviceIter, i);
                context.Save();
                {
                    Gdk.Pixbuf pixbuf = (Gdk.Pixbuf)deviceStore.GetValue(deviceIter, 2);
                    pixbuf = pixbuf.ScaleSimple(pixbuf.Width / 2, pixbuf.Height / 2, Gdk.InterpType.Bilinear);
                    context.MoveTo(0, y - pixbuf.Height);
                    Gdk.CairoHelper.SetSourcePixbuf(context, pixbuf, 0, y);
                    context.Paint();
                    if (pixbuf.Height > rowHeight) {
                        rowHeight = pixbuf.Height;
                    }
                }
                context.Restore();
                context.Save();
                {
                    DeviceInformation device = (DeviceInformation)deviceStore.GetValue(deviceIter, 0);
                    context.MoveTo(50, y + rowHeight / 2);
                    context.ShowText(String.Format("{0} ({1})", device.Name.Tr, device.Manufacturer.Name));
                }
                context.Restore();
                y += rowHeight;
            }
            print.Dispose();
        }

        private void EndPrint(object o, EndPrintArgs args)
        {
        }
    }
}
