using Datacollector.Ui.Model;
using Internationalization;
using ProbeNet.DeviceDiscovery;
using ProbeNet.Messages.Raw;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// New data source widget for ProbeNet sources.
    /// </summary>
    public class NewProbeNetDataSourceWidget : Gtk.VBox
    {
        /// <summary>
        /// Delegate method for device events.
        /// </summary>
        public delegate void DeviceDescriptionDelegate(Dictionary<string, DeviceDescription> selectedDevice);
        /// <summary>
        /// Occurs when device activated.
        /// </summary>
        public event DeviceDescriptionDelegate DeviceActivated;
        /// <summary>
        /// Occurs when device selection changed.
        /// </summary>
        public event DeviceDescriptionDelegate DeviceSelectionChanged;

        private ProbeNetDataSourceListView view;
        private DeviceFinder finder;
        private Discovery discovery;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.NewProbeNetDataSourceWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public NewProbeNetDataSourceWidget(I18n i18n, IEnumerable<Guid> configuredDataSources)
        {
            discovery = new Discovery(InternetProtocol.V4);
            finder = new DeviceFinder(discovery);
            ProbeNetDeviceDescriptionStore store = new ProbeNetDeviceDescriptionStore(i18n,
                new DeviceObserver(i18n.Language, finder), i18n.Language, configuredDataSources);
            view = new ProbeNetDataSourceListView(i18n, store);
            view.Selection.Changed += HandleSelectionChanged;
            view.RowActivated += HandleTreeRowActivated;
            PackStart(view, true, true, 0);
            finder.Enabled = discovery.HasValidConnection;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has valid connection.
        /// </summary>
        /// <value><c>true</c> if this instance has valid connection; otherwise, <c>false</c>.</value>
        public bool HasValidConnection
        {
            get
            {
                return discovery.HasValidConnection;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="Datacollector.Ui.Widgets.NewProbeNetDataSourceWidget"/> search enabled.
        /// </summary>
        /// <value><c>true</c> if search enabled; otherwise, <c>false</c>.</value>
        public bool SearchEnabled
        {
            get
            {
                return finder.Enabled;
            }
            set
            {
                finder.Enabled = value;
            }
        }

        /// <summary>
        /// Gets the selected device.
        /// </summary>
        /// <value>The selected device.</value>
        public Dictionary<string, DeviceDescription> SelectedDevice
        {
            get
            {
                return view.SelectedDevice;
            }
        }

        /// <summary>
        /// Gets the selected address.
        /// </summary>
        /// <value>The selected address.</value>
        public string SelectedAddress
        {
            get
            {
                return view.SelectedAddress;
            }
        }

        /// <summary>
        /// Gets the selected port.
        /// </summary>
        /// <value>The selected port.</value>
        public int SelectedPort
        {
            get
            {
                return view.SelectedPort;
            }
        }

        private void HandleSelectionChanged(object sender, EventArgs e)
        {
            if (DeviceSelectionChanged != null) {
                DeviceSelectionChanged(SelectedDevice);
            }
        }

        private void HandleTreeRowActivated(object o, Gtk.RowActivatedArgs args)
        {
            if (DeviceActivated != null) {
                DeviceActivated(SelectedDevice);
            }
        }
    }
}