using Frank.Widgets;
using Internationalization;
using InternationalizationUi;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// List view for manufacturers.
    /// </summary>
    public class ManufacturerListView : Gtk.TreeView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.ManufacturerListView"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="manufacturerModel">Manufacturer model.</param>
        public ManufacturerListView(I18n i18n, Gtk.ListStore manufacturerModel) :
            base()
        {
            Initializer.Initialize(i18n);
            Model = manufacturerModel;

            CellRendererIconOrText testRenderer = new CellRendererIconOrText();

            Gtk.TreeViewColumn manufacturerNameColumn = new Gtk.TreeViewColumn();
            manufacturerNameColumn.Widget = new TranslatableLabel(i18n, Constants.UiDomain, "Manufacturer");
            manufacturerNameColumn.Widget.ShowAll();
            manufacturerNameColumn.PackStart(testRenderer, false);
            manufacturerNameColumn.AddAttribute(testRenderer, "text", 1);
            manufacturerNameColumn.AddAttribute(testRenderer, "pixbuf", 2);

            AppendColumn(manufacturerNameColumn);
            Shown += HandleShown;
        }

        private void HandleShown(object sender, EventArgs e)
        {
            Gtk.TreeIter iter;
            Model.IterNthChild(out iter, Model.IterNChildren() - 1);
            if ((Model as Gtk.ListStore).IterIsValid(iter)) {
                Selection.SelectIter(iter);
            } else {
                Logging.Logger.Warning("Could not select last manufacturer because iter is not valid.");
            }
        }
    }
}

