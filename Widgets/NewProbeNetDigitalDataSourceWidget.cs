﻿using Datacollector.Ui.Model;
using Internationalization;
using ProbeNet.DeviceDiscovery;
using ProbeNet.Messages.Raw;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Datacollector.Ui.Widgets
{
    public class NewProbeNetDigitalDataSourceWidget : Gtk.VBox
    {
        /// <summary>
        /// Delegate method for device events.
        /// </summary>
        public delegate void DeviceDescriptionDelegate(Dictionary<string, DeviceDescription> selectedDevice);
        /// <summary>
        /// Occurs when device activated.
        /// </summary>
        public event DeviceDescriptionDelegate DeviceActivated;
        /// <summary>
        /// Occurs when device selection changed.
        /// </summary>
        public event DeviceDescriptionDelegate DeviceSelectionChanged;

        private ProbeNetDataSourceListView view;
        private IEnumerable<Guid> configuredDataSources;
        private DeviceDescriptionStore cached;
        private TranslationString empty;

        public NewProbeNetDigitalDataSourceWidget(I18n i18n, IEnumerable<Guid> configuredDataSources, DeviceDescriptionStore cached)
        {
            this.empty = new TranslationString(i18n, Constants.UiDomain, "");
            this.configuredDataSources = configuredDataSources;
            this.cached = cached;
            DigitalDeviceFinder = ProbeNetDigitalDeviceFinder.GetInstance();
            DeviceDescriptionStore store = new DeviceDescriptionStore(i18n);
            view = new ProbeNetDataSourceListView(i18n, store);
            view.Selection.Changed += HandleSelectionChanged;
            view.RowActivated += HandleTreeRowActivated;
            PackStart(view, true, true, 0);
            DigitalDeviceFinder.DeviceDescriptionReceived += FinderDeviceDescriptionReceived;
            if (cached != null) {
                foreach (var row in cached) {
                    bool configured = configuredDataSources.Contains(row.Item1);
                    view.Model.Add(row.Item1, row.Item2, row.Item3, row.Item4, row.Item5, row.Item6, row.Item7,
                        !configured, configured ? view.Model.WarningConfiguredText : view.Model.WarningCachedText);
                }
            }
        }

        private void FinderDeviceDescriptionReceived(string language, string address, int port, DeviceDescription deviceDescription)
        {
            if (view.Model == null) {
                return;
            }
            Gdk.Pixbuf icon = null;
            if (deviceDescription.Icon != null) {
                icon = new Gdk.Pixbuf(deviceDescription.Icon);
            }
            int index = view.Model.FindDevice(deviceDescription.Uuid);
            bool configured = configuredDataSources.Contains(deviceDescription.Uuid);
            if (index < 0) {
                view.Model.Add(
                    deviceDescription.Uuid,
                    new Dictionary<string, DeviceDescription>() { { language, deviceDescription } },
                    deviceDescription.Caption,
                    deviceDescription.SerialNumber,
                    address,
                    port,
                    icon,
                    configured,
                    configured ? view.Model.WarningConfiguredText : empty);
            } else { // update existing information
                view.Model[index].Item1 = deviceDescription.Uuid;
                if (view.Model[index].Item2.ContainsKey(language)) {
                    view.Model[index].Item2[language] = deviceDescription;
                } else {
                    view.Model[index].Item2.Add(language, deviceDescription);
                }
                view.Model[index].Item3 = deviceDescription.Caption;
                view.Model[index].Item4 = deviceDescription.SerialNumber;
                if (address != null) {
                    view.Model[index].Item5 = address;
                }
                view.Model[index].Item6 = port;
                if (icon != null) {
                    view.Model[index].Item7 = icon;
                }
                view.Model[index].Item8 = !configured;
                view.Model[index].Item9 = configured ? view.Model.WarningConfiguredText : empty;    // remove "cached", but null is rejected by GTK here
            }
        }

        /// <summary>
        /// Gets the selected device.
        /// </summary>
        /// <value>The selected device.</value>
        public Dictionary<string, DeviceDescription> SelectedDevice
        {
            get
            {
                return view.SelectedDevice;
            }
        }

        /// <summary>
        /// Gets the selected address.
        /// </summary>
        /// <value>The selected address.</value>
        public string SelectedAddress
        {
            get
            {
                return view.SelectedAddress;
            }
        }

        /// <summary>
        /// Gets the selected port.
        /// </summary>
        /// <value>The selected port.</value>
        public int SelectedPort
        {
            get
            {
                return view.SelectedPort;
            }
        }

        public ProbeNetDigitalDeviceFinder DigitalDeviceFinder
        {
            get;
            private set;
        }

        private void HandleSelectionChanged(object sender, EventArgs e)
        {
            if (SelectedDevice != null && DeviceSelectionChanged != null) {
                DeviceSelectionChanged(SelectedDevice);
            }
        }

        private void HandleTreeRowActivated(object o, Gtk.RowActivatedArgs args)
        {
            if (SelectedDevice != null && DeviceActivated != null) {
                DeviceActivated(SelectedDevice);
            }
        }

        public DeviceDescriptionStore FoundDevices
        {
            get
            {
                return view.Model;
            }
        }
    }
}
