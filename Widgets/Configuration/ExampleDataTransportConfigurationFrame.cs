/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Datacollector.Transportation;
using Internationalization;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame for example data transport.
    /// </summary>
    public class ExampleDataTransportConfigurationFrame : GenericTransportConfigurationFrame<ExampleDataTransport>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.ExampleDataTransportConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
        public ExampleDataTransportConfigurationFrame(I18n i18n, ExampleDataTransport transport) :
            base(i18n, transport, null)
        {
            Initializer.Initialize(i18n);
        }

        /// <inheritdoc/>
        public override void Save()
        {
        }

        // <inheritdoc/>
        public override void Check()
        {
        }
    }
}
