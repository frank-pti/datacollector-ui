using Datacollector.Transportation;
using Internationalization;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Factory for transport configuration frame.
    /// </summary>
    public class TransportConfigurationFrameFactory
    {
        private I18n i18n;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.TransportConfigurationFrameFactory"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public TransportConfigurationFrameFactory(I18n i18n)
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
        }

        /// <summary>
        /// Produces the frame for specified transport.
        /// </summary>
        /// <returns>The frame.</returns>
        /// <param name="transport">Transport.</param>
        /// <param name="invalidItems">The list of invalid items.</param>
        public TransportConfigurationFrame ProduceFrame(Transport transport, List<string> invalidItems)
        {
            if (transport is SerialTransport) {
                return new SerialTransportConfigurationFrame(i18n, transport as SerialTransport, invalidItems);
            }
            if (transport is TcpServerTransport) {
                return new TcpServerTransportConfigurationFrame(i18n, transport as TcpServerTransport, invalidItems);
            }
            if (transport is TcpClientTransport) {
                return new TcpClientTransportConfigurationFrame(i18n, transport as TcpClientTransport, invalidItems);
            }
            if (transport is UdpServerTransport) {
                return new UdpClientTransportConfigurationFrame(i18n, transport as UdpServerTransport, invalidItems);
            }
            if (transport is ExampleDataTransport) {
                return new ExampleDataTransportConfigurationFrame(i18n, transport as ExampleDataTransport);
            }
            throw new ArgumentException(String.Format(
                "Unknown type: {0}", transport.GetType().Name), "transport");
        }
    }
}
