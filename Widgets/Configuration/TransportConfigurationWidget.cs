/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Datacollector.Sources;
using Internationalization;
using Datacollector.Transportation;
using Datacollector.Configuration;
using System.Collections.Generic;
using Gtk;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Transport configuration widget.
    /// </summary>
    public class TransportConfigurationWidget<D> : Gtk.VBox
        where D : DataSourceSettings
    {
        /// <summary>
        /// Delegate method for data requested event.
        /// </summary>
        public delegate void DeleteDataRequestedDelegate(bool delete);
        /// <summary>
        /// Occurs when delete data requested.
        /// </summary>
        public event DeleteDataRequestedDelegate DeleteDataRequested;

        /// <summary>
        /// Delegate method for the display alert message request
        /// </summary>
        /// <param name="message">The alert text to be displayed</param>
        public delegate void DisplayAlertRequestDelegate(TranslationString message, ResponseType? disableResponse);
        /// <summary>
        /// Occurs when an alert message should be displayed
        /// </summary>
        public event DisplayAlertRequestDelegate DisplayAlertRequest;

        /// <summary>
        /// Delegate method for the hide alert message request
        /// </summary>
        public delegate void HideAlertRequestDelegate(ResponseType? enableResponse);
        /// <summary>
        /// Occurs when an alert message should be hidden
        /// </summary>
        public event HideAlertRequestDelegate HideAlertRequest;

        private TransportConfigurationFrameFactory frameFactory;
        private TransportConfigurationFrame frame;
        private DataSource<D> dataSource;
        private List<string> invalidItems;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.TransportConfigurationWidget`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dataSource">Data source.</param>
        public TransportConfigurationWidget(I18n i18n, DataSource<D> dataSource, List<string> invalidItems)
        {
            Initializer.Initialize(i18n);
            this.dataSource = dataSource;
            this.invalidItems = invalidItems;
            frameFactory = new TransportConfigurationFrameFactory(i18n);
            frame = frameFactory.ProduceFrame(dataSource.Transport, invalidItems);
            frame.TransportTypeSelected += HandleTransportTypeSelected;
            frame.DisplayAlertRequest += HandleDisplayAlertRequested;
            frame.HideAlertRequest += HandleHideAlertRequested;
            Add(frame);
        }

        private TransportConfigurationFrame Frame
        {
            get
            {
                return frame;
            }
            set
            {
                if (frame != value) {
                    frame.TransportTypeSelected -= HandleTransportTypeSelected;
                    frame.DisplayAlertRequest -= HandleDisplayAlertRequested;
                    frame.HideAlertRequest -= HandleHideAlertRequested;
                    frame = value;
                    frame.TransportTypeSelected += HandleTransportTypeSelected;
                    frame.DisplayAlertRequest += HandleDisplayAlertRequested;
                    frame.HideAlertRequest += HandleHideAlertRequested;
                    Gtk.Application.Invoke(delegate {
                        if (this.Children != null && this.Children[0] != null) {
                            Remove(this.Children[0]);
                            Add(frame);
                            frame.ShowAll();
                        }
                    });
                }
            }
        }

        private void HandleTransportTypeSelected(Type type)
        {
            Transport newTransport = TransportFactory.BuildTransport(type, dataSource.DeviceHandle);
            Frame = frameFactory.ProduceFrame(newTransport, invalidItems);
            Frame.Check();
            NotifyDeleteDataRequested(newTransport.MustDeleteDataBeforeUse);
        }

        /// <summary>
        /// Save the configuration.
        /// </summary>
        public void Save()
        {
            Frame.Save();
        }

        /// <summary>
        /// Gets the transport.
        /// </summary>
        /// <value>The transport.</value>
        public Transport Transport
        {
            get
            {
                return Frame.Transport;
            }
        }

        private void NotifyDeleteDataRequested(bool delete)
        {
            if (DeleteDataRequested != null) {
                DeleteDataRequested(delete);
            }
        }

        private void HandleDisplayAlertRequested(TranslationString message, ResponseType? disableResponse)
        {
            if (DisplayAlertRequest != null) {
                DisplayAlertRequest(message, disableResponse);
            }
        }

        private void HandleHideAlertRequested(ResponseType? enableResponse)
        {
            if (HideAlertRequest != null) {
                HideAlertRequest(enableResponse);
            }
        }
    }
}
