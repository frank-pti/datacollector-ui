/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Datacollector.Transportation;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame for generic transports.
    /// </summary>
    public abstract class GenericTransportConfigurationFrame<T> : TransportConfigurationFrame
        where T : Transport
    {
        private T transport;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.GenericTransportConfigurationFrame`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
        protected GenericTransportConfigurationFrame(I18n i18n, T transport, List<string> invalidItems) :
            base(i18n, i18n.TrObject(Constants.UiDomain, "Connection"), typeof(T), invalidItems)
        {
            Initializer.Initialize(i18n);
            this.transport = transport;
        }

        /// <summary>
        /// Gets the generic transport.
        /// </summary>
        /// <value>The generic transport.</value>
        protected T GenericTransport
        {
            get
            {
                return transport;
            }
        }

        /// <inheritdoc/>
        public override Transport Transport
        {
            get
            {
                return transport;
            }
        }
    }
}
