using Datacollector.Transportation;
using Internationalization;
using InternationalizationUi;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame for TCP server.
    /// </summary>
    public class TcpServerTransportConfigurationFrame : GenericTransportConfigurationFrame<TcpServerTransport>
    {
        private Gtk.SpinButton portSpinButton;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.TcpServerTransportConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
        public TcpServerTransportConfigurationFrame(I18n i18n, TcpServerTransport transport, List<string> invalidItems) :
            base(i18n, transport, invalidItems)
        {
            Initializer.Initialize(i18n);
            TranslatableLabel portLabel = new TranslatableLabel(i18n, Constants.UiDomain, "TCP Port");
            portLabel.Xalign = 0;

            portSpinButton = new Gtk.SpinButton(1, 0xFFFF, 1);
            portSpinButton.Value = GenericTransport.Port;
            portSpinButton.ValueChanged += (o, args) => Check();

            AddConfigurationRow(portLabel, portSpinButton);
        }

        /// <inheritdoc/>
        public override void Save()
        {
            GenericTransport.Port = portSpinButton.ValueAsInt;
        }

        /// <inheritdoc/>
        public override void Check()
        {
            int port = portSpinButton.ValueAsInt;
            string combined = String.Format("{0}", port);
            if (InvalidItems.Contains(combined)) {
                NotifyDisplayAlertRequest(i18n.TrObject(Constants.UiDomain, "TCP port {0} is already used.", port));
            } else {
                NotifyHideAlertRequest();
            }
        }
    }
}
