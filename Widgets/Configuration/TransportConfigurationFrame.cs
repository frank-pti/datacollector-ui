using Datacollector.Transportation;
using Gtk;
using Internationalization;
using InternationalizationUi;
using Logging;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Transport configuration frame.
    /// </summary>
    public abstract class TransportConfigurationFrame : ConfigurationFrame
    {
        private TransportComboBox transportComboBox;

        /// <summary>
        /// Delegate method for transport type selected event.
        /// </summary>
        public delegate void TransportTypeSelectedDelegate(Type type);
        /// <summary>
        /// Occurs when transport type selected.
        /// </summary>
        public event TransportTypeSelectedDelegate TransportTypeSelected;

        /// <summary>
        /// Delegate method for the display alert message request
        /// </summary>
        /// <param name="message">The alert text to be displayed</param>
        public delegate void DisplayAlertRequestDelegate(TranslationString message, ResponseType? disableResponse);
        /// <summary>
        /// Occurs when an alert message should be displayed
        /// </summary>
        public event DisplayAlertRequestDelegate DisplayAlertRequest;

        /// <summary>
        /// Delegate method for the hide alert message request
        /// </summary>
        public delegate void HideAlertRequestDelegate(ResponseType? enableResponse);
        /// <summary>
        /// Occurs when an alert message should be hidden
        /// </summary>
        public event HideAlertRequestDelegate HideAlertRequest;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.TransportConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="title">Title.</param>
        /// <param name="transportType">Transport type.</param>
        protected TransportConfigurationFrame(I18n i18n, TranslationString title, Type transportType, List<string> invalidItems) :
            base(i18n, title)
        {
            Initializer.Initialize(i18n);
            if (invalidItems == null) {
                invalidItems = new List<string>();
            }
            InvalidItems = invalidItems;
            TranslatableLabel transportLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Connection method");
            transportComboBox = new TransportComboBox(i18n, TransportFactory.GetIdentifierOfTransportType(transportType));
            transportComboBox.TransportTypeSelected += HandleTransportComboBoxTransportTypeSelected;
            AddConfigurationRow(transportLabel, transportComboBox);
        }

        private void HandleTransportComboBoxTransportTypeSelected(Type type)
        {
            try {
                NotifyTransportTypeSelected(type);
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        /// <summary>
        /// Gets the transport.
        /// </summary>
        /// <value>The transport.</value>
        public abstract Transport Transport
        {
            get;
        }

        /// <summary>
        /// Check the current settings if they are not contained in the list of invalid items
        /// </summary>
        /// <remarks>Use the <see cref="NotifyDisplayAlertRequest(TranslationString)"/> method for displaying an
        /// alert if check failed or <see cref="NotifyHideAlertRequest()"/> to hide alert if check succeeded.</remarks>
        public abstract void Check();

        /// <summary>
        /// List of items that are not valid (e.g. because they are already used in other transport settings)
        /// </summary>
        protected List<string> InvalidItems
        {
            get;
            private set;
        }

        private void NotifyTransportTypeSelected(Type type)
        {
            if (TransportTypeSelected != null) {
                TransportTypeSelected(type);
            }
        }

        protected void NotifyDisplayAlertRequest(TranslationString message, ResponseType? disableResponse = Gtk.ResponseType.Accept)
        {
            if (DisplayAlertRequest != null) {
                DisplayAlertRequest(message, disableResponse);
            }
        }

        protected void NotifyHideAlertRequest(ResponseType? enableResponse = Gtk.ResponseType.Accept)
        {
            if (HideAlertRequest != null) {
                HideAlertRequest(enableResponse);
            }
        }
    }
}

