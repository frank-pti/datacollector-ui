/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Datacollector.Transportation;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Combo box for selecting the transport type.
    /// </summary>
	public class TransportComboBox: Gtk.ComboBox
	{
		private I18n i18n;

		private const int NameColumn = 0;
		private const int TypeColumn = 1;
		private const int IdentifierColumn = 2;

        /// <summary>
        /// Delegate method for transport type selected delegate.
        /// </summary>
		public delegate void TransportTypeSelectedDelegate(Type type);
        /// <summary>
        /// Occurs when transport type selected.
        /// </summary>
		public event TransportTypeSelectedDelegate TransportTypeSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.Configuration.TransportComboBox"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="selectedIdentifier">Selected identifier.</param>
		public TransportComboBox(I18n i18n, string selectedIdentifier): base(LoadModel (i18n))
		{
            Initializer.Initialize(i18n);
			this.i18n = i18n;
			i18n.LanguageChanged += HandleI18nLanguageChanged;

			Gtk.CellRendererText nameRenderer = new Gtk.CellRendererText();
			PackStart(nameRenderer, true);
			AddAttribute(nameRenderer, "text", NameColumn);

			SelectEntry(selectedIdentifier);

			Changed += HandleChanged;
		}

		private void HandleChanged (object sender, EventArgs e)
		{
			NotifyTransportTypeSelected(SelectedTransportType);
		}

        /// <summary>
        /// Gets the type of the selected transport.
        /// </summary>
        /// <value>The type of the selected transport.</value>
		public Type SelectedTransportType {
			get {
				Gtk.TreeIter iter;
				GetActiveIter (out iter);
				return Model.GetValue (iter, TypeColumn) as Type;
			}
		}

		private void SelectEntry(string identifier)
		{
			Gtk.ListStore listStore = Model as Gtk.ListStore;
			Gtk.TreeIter iter;
			listStore.GetIterFirst(out iter);
			MoveIterToType (ref iter, identifier);
			if (!listStore.IterIsValid(iter)) {
				listStore.GetIterFirst(out iter);
			}
			SetActiveIter(iter);
		}

		private void HandleI18nLanguageChanged (I18n source, string language)
		{
			Gtk.Application.Invoke (delegate {
				Gtk.TreeIter iter;
				Model.GetIterFirst (out iter);
				do {
					Type type = Model.GetValue (iter, TypeColumn) as Type;
					string name = TransportFactory.TransportTypeName(i18n, type).Tr;
					Model.SetValue (iter, NameColumn, name);
				} while (Model.IterNext(ref iter));
			});
		}

		private void MoveIterToType (ref Gtk.TreeIter iter, string identifier)
		{
			while (Model.IterNext(ref iter) &&
				(Model.GetValue(iter, IdentifierColumn) as string) != identifier)
			{
				// nop
			}
		}

		private static Gtk.ListStore LoadModel(I18n i18n)
		{
			Gtk.ListStore store = new Gtk.ListStore(typeof(string), typeof(Type), typeof(string));
			foreach (Type type in TransportFactory.TransportTypes) {
				store.AppendValues(
					TransportFactory.TransportTypeName(i18n, type).Tr,
					type,
					TransportFactory.GetIdentifierOfTransportType(type));
			}
			return store;
		}

		private void NotifyTransportTypeSelected(Type type)
		{
			if (TransportTypeSelected != null) {
				TransportTypeSelected(type);
			}
		}
	}
}
