using Datacollector.Transportation;
using Internationalization;
using InternationalizationUi;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame for TPC clients.
    /// </summary>
    public class TcpClientTransportConfigurationFrame : GenericTransportConfigurationFrame<TcpClientTransport>
    {
        private Gtk.SpinButton portSpinButton;
        private Gtk.Entry addressEntry;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.TcpClientTransportConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
        public TcpClientTransportConfigurationFrame(I18n i18n, TcpClientTransport transport, List<string> invalidItems) :
            base(i18n, transport, invalidItems)
        {
            Initializer.Initialize(i18n);
            TranslatableLabel portLabel = new TranslatableLabel(i18n, Constants.UiDomain, "TCP Port");
            portLabel.Xalign = 0;
            portSpinButton = new Gtk.SpinButton(1, 0xFFFF, 1);
            portSpinButton.Value = GenericTransport.Port;
            portSpinButton.ValueChanged += (o, args) => Check();

            AddConfigurationRow(portLabel, portSpinButton);

            TranslatableLabel addressLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Address");
            addressLabel.Xalign = 0;

            addressEntry = new Gtk.Entry();
            addressEntry.Text = GenericTransport.Address;
            addressEntry.Changed += (o, args) => Check();

            AddConfigurationRow(addressLabel, addressEntry);
        }

        /// <inheritdoc/>
        public override void Save()
        {
            GenericTransport.Port = portSpinButton.ValueAsInt;
            GenericTransport.Address = addressEntry.Text;
        }

        /// <inheritdoc/>
        public override void Check()
        {
            string address = addressEntry.Text;
            int port = portSpinButton.ValueAsInt;
            string combined = String.Format("{0}:{1}", address, port);
            if (InvalidItems.Contains(combined)) {
                NotifyDisplayAlertRequest(i18n.TrObject(Constants.UiDomain, "TCP address '{0}' with port {1} is already used.", address, port));
            } else {
                NotifyHideAlertRequest();
            }
        }
    }
}
