/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Datacollector.Configuration;
using Internationalization;
using InternationalizationUi;
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Data source settings frame.
    /// </summary>
    public class DataSourceSettingsFrame : ConfigurationFrame
    {
        /// <summary>
        /// Delegate method for data requested event.
        /// </summary>
        public delegate void DeleteDataRequestedDelegate(bool delete);
        /// <summary>
        /// Occurs when delete data requested.
        /// </summary>
        public event DeleteDataRequestedDelegate DeleteDataRequested;
        /// <summary>
        /// Delegate method for set confirm active event
        /// </summary>
        public delegate void SetConfirmActiveDelegate(bool active);
        /// <summary>
        /// Occurs when set confirm active.
        /// </summary>
        public event SetConfirmActiveDelegate SetConfirmActive;

        private DataSourceSettings settings;

        private TranslatableLabel typeValueLabel;
        private Gtk.Entry nameEntry;
        private Gtk.Entry serialNumberEntry;
        private TranslatableCheckButton dumpDataCheckButton;
        private TranslatableIconLabel nameWarnLabel;
        private bool nameWarnLabelVisible;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.DataSourceSettingsFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="settings">Settings.</param>
        /// <param name="deviceTypeName">Device type name.</param>
        /// <param name="icon">Icon.</param>
        /// <param name="deviceInformation">Device information.</param>
        public DataSourceSettingsFrame(
            I18n i18n,
            DataSourceSettings settings,
            string deviceTypeName,
            Gdk.Pixbuf icon,
            TranslationString deviceInformation) :
            base(i18n, i18n.TrObject(Constants.UiDomain, "Data Source"))
        {
            Initializer.Initialize(i18n);
            this.settings = settings;

            Gtk.Image iconWidget = new Gtk.Image(icon);
            Gtk.HBox iconBox = new Gtk.HBox(false, 5);
            iconBox.PackStart(iconWidget, false, false, 0);
            AddConfigurationRow(iconBox);

            if (deviceInformation != null) {
                TranslatableIconLabel label = new TranslatableIconLabel(i18n.TrObject(Constants.UiDomain, "<b>{0}</b>", deviceInformation), new Gtk.Image(Gtk.Stock.DialogInfo, Gtk.IconSize.Dialog)) {
                    UseMarkup = true,
                    Wrap = true,
                    LineWrapMode = Pango.WrapMode.Word,
                    Spacing = 15
                };
                AddConfigurationRow(label);
            }

            TranslatableLabel typeCaptionLabel =
                new TranslatableLabel(i18n, Constants.UiDomain, "Type") {
                    Xalign = 0f
                };
            typeValueLabel = new TranslatableLabel(i18n, Constants.UiDomain, "{0}", deviceTypeName) {
                Xalign = 0f
            };
            Gtk.EventBox eventBox = new Gtk.EventBox();
            eventBox.Add(typeValueLabel);
            eventBox.AddEvents((int)Gdk.EventMask.ButtonPressMask);
            eventBox.ButtonPressEvent += HandleTypeValueLabelButtonPressEvent;
            AddConfigurationRow(typeCaptionLabel, eventBox);

            TranslatableLabel nameLabel =
                new TranslatableLabel(i18n, Constants.UiDomain, "Name") {
                    Xalign = 0f
                };
            nameEntry = new Gtk.Entry() {
                Text = settings.Name
            };
            nameEntry.Changed += HandleNameChanged;
            AddConfigurationRow(nameLabel, nameEntry);

            Gtk.Image nameWarnImage = new Gtk.Image(Gtk.Stock.DialogWarning, Gtk.IconSize.Button);
            string invalidCharacters = String.Join(" ", BuildInvalidPrintableFilenameChars());
            nameWarnLabel = new TranslatableIconLabel(
                i18n.TrObject(Constants.UiDomain, "The name contains characters which can not be used in file paths like {0}", invalidCharacters),
                nameWarnImage) {
                    UseMarkup = false,
                    Wrap = true,
                    LineWrapMode = Pango.WrapMode.Word,
                    Spacing = 15
                };
            nameWarnLabel.ShowAll();
            nameWarnLabel.Visible = false;
            nameWarnLabel.NoShowAll = true;
            AddConfigurationRow(nameWarnLabel);

            TranslatableLabel serialNumberLabel =
                new TranslatableLabel(i18n, Constants.UiDomain, "Serial number") {
                    Xalign = 0f
                };
            serialNumberEntry = new Gtk.Entry() {
                Text = settings.SerialNumber
            };
            AddConfigurationRow(serialNumberLabel, serialNumberEntry);

            dumpDataCheckButton = new TranslatableCheckButton(
                i18n, Constants.UiDomain, "Dump incoming data to log directory") {
                    Active = settings.DumpIncomingData
                };
            AddConfigurationRow(dumpDataCheckButton);
        }

        private void HandleTypeValueLabelButtonPressEvent(object sender, Gtk.ButtonPressEventArgs args)
        {
            if (args.Event.Type == Gdk.EventType.TwoButtonPress) {
                Gtk.Application.Invoke((o, e) => nameEntry.Text = typeValueLabel.Text);
                nameEntry.SelectRegion(0, nameEntry.Text.Length);
            }
        }

        private List<char> BuildInvalidPrintableFilenameChars()
        {
            List<char> chars = new List<char>(System.IO.Path.GetInvalidFileNameChars());
            for (int i = 0; i < chars.Count; ) {
                if (chars[i] < 0x20 || chars[i] > 127) {
                    chars.RemoveAt(i);
                } else {
                    i++;
                }
            }
            return chars;
        }

        private void UpdateNameWarnLabelVisible()
        {
            bool visible = nameEntry.Text.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0;
            if (visible != nameWarnLabelVisible) {
                nameWarnLabelVisible = visible;
                Gtk.Application.Invoke(delegate {
                    nameWarnLabel.Visible = visible;
                });
                NotifySetConfirmActive(!nameWarnLabelVisible);
            }
        }

        private void HandleNameChanged(object sender, EventArgs e)
        {
            UpdateNameWarnLabelVisible();
        }

        /// <inheritdoc/>
        public override void Save()
        {
            settings.Name = nameEntry.Text;
            if (settings.SerialNumber == SerialNumberForceDelete && settings.SerialNumber != serialNumberEntry.Text) {
                NotifyDeleteDataRequested(true);
            }
            settings.SerialNumber = serialNumberEntry.Text;
            settings.DumpIncomingData = dumpDataCheckButton.Active;
        }

        /// <summary>
        /// Gets the data source settings.
        /// </summary>
        /// <value>The data source settings.</value>
        public DataSourceSettings DataSourceSettings
        {
            get
            {
                return settings;
            }
        }

        /// <summary>
        /// A special serial number that requires received data to be deleted if the serial number is changed.
        /// </summary>
        public string SerialNumberForceDelete
        {
            get;
            set;
        }

        private void NotifyDeleteDataRequested(bool delete)
        {
            if (DeleteDataRequested != null) {
                DeleteDataRequested(delete);
            }
        }

        private void NotifySetConfirmActive(bool active)
        {
            if (SetConfirmActive != null) {
                SetConfirmActive(active);
            }
        }
    }
}

