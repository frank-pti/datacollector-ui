/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;
using Frank.Widgets;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame.
    /// </summary>
	public abstract class ConfigurationFrame: TranslatableFrame
	{
		private Gtk.Table configurationTable;
		
		private Gtk.AttachOptions attachOptions =
			Gtk.AttachOptions.Shrink | Gtk.AttachOptions.Fill;
		
		private const int XTablePadding = 5;
		private const int YTablePadding = 3;
		
        /// <summary>
        /// The i18n.
        /// </summary>
		protected I18n i18n;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.Configuration.ConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="title">Title.</param>
		public ConfigurationFrame(I18n i18n, TranslationString title):
			base(title)
		{
            Initializer.Initialize(i18n);
			this.i18n = i18n;
			
			Build();
		}
		
		private void Build()
		{
			Gtk.VBox box = new Gtk.VBox();
			Gtk.Alignment alignment = new FrameAlignment(5);
			alignment.Add(box);
			Add(alignment);
			
			configurationTable = new Gtk.Table(0, 2, false);
			box.PackStart(configurationTable, true, true, 0);
		}

        /// <summary>
        /// Adds the configuration row.
        /// </summary>
        /// <param name="widget">Widget.</param>
		protected void AddConfigurationRow(Gtk.Widget widget)
		{
            AddConfigurationRow(widget, attachOptions);
		}

        /// <summary>
        /// Adds the configuration row.
        /// </summary>
        /// <param name="widget">Widget.</param>
        /// <param name="yOptions">Y options.</param>
        protected void AddConfigurationRow(Gtk.Widget widget, Gtk.AttachOptions yOptions)
        {
            uint currentRow = configurationTable.NRows;
            configurationTable.Attach(widget, 0, 2, currentRow, currentRow + 1,
                attachOptions, yOptions, XTablePadding, YTablePadding);
        }
		
        /// <summary>
        /// Adds the configuration row.
        /// </summary>
        /// <param name="widget1">Widget1.</param>
        /// <param name="widget2">Widget2.</param>
		protected void AddConfigurationRow(Gtk.Widget widget1, Gtk.Widget widget2)
		{
            AddConfigurationRow(widget1, widget2, attachOptions);
		}

        /// <summary>
        /// Adds the configuration row.
        /// </summary>
        /// <param name="widget1">Widget1.</param>
        /// <param name="widget2">Widget2.</param>
        /// <param name="yOptions">Y options.</param>
        protected void AddConfigurationRow(Gtk.Widget widget1, Gtk.Widget widget2, Gtk.AttachOptions yOptions)
        {
            uint currentRow = configurationTable.NRows;
            configurationTable.Attach(widget1, 0, 1, currentRow, currentRow + 1,
                attachOptions, yOptions, XTablePadding, YTablePadding);
            configurationTable.Attach(widget2, 1, 2, currentRow, currentRow + 1,
                attachOptions, yOptions, XTablePadding, YTablePadding);
        }

        /// <summary>
        /// Save configuration.
        /// </summary>
		public abstract void Save();
	}
}

