using Datacollector.Configuration;
using Datacollector.Sources;
using Frank.Widgets.Dialogs;
using Internationalization;
using Logging;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration page.
    /// </summary>
    public class ConfigurationPage<T> : Gtk.VBox
        where T : DataSourceSettings
    {
        /// <summary>
        /// Delegate method for the display alert message request
        /// </summary>
        /// <param name="message">The alert text to be displayed</param>
        public delegate void DisplayAlertRequestDelegate(TranslationString message, Gtk.ResponseType? disableResponse);
        /// <summary>
        /// Occurs when an alert message should be displayed
        /// </summary>
        public event DisplayAlertRequestDelegate DisplayAlertRequest;

        /// <summary>
        /// Delegate method for the hide alert message request
        /// </summary>
        public delegate void HideAlertRequestDelegate(Gtk.ResponseType? enableResponse);
        /// <summary>
        /// Occurs when an alert message should be hidden
        /// </summary>
        public event HideAlertRequestDelegate HideAlertRequest;

        private DataSourceSettingsFrame dataSourceSettingsFrame;
        private TransportConfigurationWidget<T> transportConfiguration;

        private I18n i18n;
        private DataSource<T> dataSource;
        private bool deleteDataOnSave;
        private bool dataPresent;

        private Gtk.VBox box;
        private Gtk.Window dialogParent;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.Configuration.ConfigurationPage`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dialogParent">Dialog parent.</param>
        /// <param name="dataSource">Data source.</param>
        /// <param name="dataSourceSettingsFrame">Data source settings frame.</param>
        /// <param name="invalidItems">List of invalid items.</param>
        /// <param name="dataPresent">If set to <c>true</c> data present.</param>
        public ConfigurationPage(
            I18n i18n,
            Gtk.Window dialogParent,
            DataSource<T> dataSource,
            DataSourceSettingsFrame dataSourceSettingsFrame,
            List<string> invalidItems,
            bool dataPresent) :
            base()
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
            this.dialogParent = dialogParent;
            this.dataPresent = dataPresent;
            deleteDataOnSave = false;
            box = new Gtk.VBox();
            PackStart(box);

            this.dataSource = dataSource;
            this.dataSourceSettingsFrame = dataSourceSettingsFrame;
            this.dataSourceSettingsFrame.DeleteDataRequested += HandleDeleteDataRequested;

            box.PackStart(dataSourceSettingsFrame, true, true, 5);

            transportConfiguration = new TransportConfigurationWidget<T>(i18n, dataSource, invalidItems);
            transportConfiguration.DeleteDataRequested += HandleDeleteDataRequested;
            transportConfiguration.DisplayAlertRequest += HandleDisplayAlertRequested;
            transportConfiguration.HideAlertRequest += HandleHideAlertRequested;
            box.PackStart(transportConfiguration, false, false, 5);
        }

        /// <summary>
        /// Handles the delete data requested.
        /// </summary>
        /// <param name="delete">If set to <c>true</c> delete.</param>
        private void HandleDeleteDataRequested(bool delete)
        {
            if (dataPresent && delete) {
                CustomMessageDialog.ShowInfoDialog(
                    dialogParent, i18n, i18n.TrObject(Constants.UiDomain, "When selecting this transport, all current data of the datasource will be deleted."));
            }
            deleteDataOnSave = delete;
        }

        private void HandleDisplayAlertRequested(TranslationString message, Gtk.ResponseType? disableResponse)
        {
            if (DisplayAlertRequest != null) {
                DisplayAlertRequest(message, disableResponse);
            }
        }

        private void HandleHideAlertRequested(Gtk.ResponseType? enableResponse)
        {
            if (HideAlertRequest != null) {
                HideAlertRequest(enableResponse);
            }
        }

        private void SaveSettings()
        {
            dataSourceSettingsFrame.Save();
            transportConfiguration.Save();
            dataSource.Transport = transportConfiguration.Transport;
        }

        /// <summary>
        /// Save this instance.
        /// </summary>
        public void Save()
        {
            try {
                if (dataSource.Active) {
                    dataSource.Active = false;
                    SaveSettings();
                    dataSource.Active = true;
                } else {
                    SaveSettings();
                }
            } catch (Exception e) {
                Logger.Log(e, "Error while saving data source configuration.");
            }
        }

        /// <summary>
        /// Gets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public DataSource<T> DataSource
        {
            get
            {
                return dataSource;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this
        /// <see cref="Datacollector.Ui.Widgets.Configuration.ConfigurationPage`1"/> delete data on save.
        /// </summary>
        /// <value><c>true</c> if delete data on save; otherwise, <c>false</c>.</value>
        public bool DeleteDataOnSave
        {
            get
            {
                return deleteDataOnSave;
            }
        }
    }
}
