﻿using Datacollector.Transportation;
using Internationalization;
using InternationalizationUi;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets.Configuration
{
    public class UdpClientTransportConfigurationFrame : GenericTransportConfigurationFrame<UdpServerTransport>
    {
        private Gtk.SpinButton portSpinButton;

        public UdpClientTransportConfigurationFrame(I18n i18n, UdpServerTransport udpServerTransport, List<string> invalidItems) :
            base(i18n, udpServerTransport, invalidItems)
        {
            Initializer.Initialize(i18n);
            TranslatableLabel portLabel = new TranslatableLabel(i18n, Constants.UiDomain, "TCP Port");
            portLabel.Xalign = 0;

            portSpinButton = new Gtk.SpinButton(0, 0xFFFF, 1);
            portSpinButton.Value = GenericTransport.Port;
            portSpinButton.ValueChanged += (o, args) => Check();

            AddConfigurationRow(portLabel, portSpinButton);
        }

        public override void Save()
        {
            GenericTransport.Port = portSpinButton.ValueAsInt;
        }

        public override void Check()
        {
        }
    }
}
