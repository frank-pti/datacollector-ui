/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Datacollector.Transportation;
using Internationalization;
using InternationalizationUi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;

namespace Datacollector.Ui.Widgets.Configuration
{
    /// <summary>
    /// Configuration frame for serial transport.
    /// </summary>
    public class SerialTransportConfigurationFrame : GenericTransportConfigurationFrame<SerialTransport>
    {
        private Gtk.ComboBoxEntry portComboBoxEntry;
        private Gtk.ComboBox baudRateComboBox;

        private readonly List<string> baudRateNames = new List<string>() {
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"
        };

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="Datacollector.Ui.Widgets.Configuration.SerialTransportConfigurationFrame"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="transport">Transport.</param>
        public SerialTransportConfigurationFrame(I18n i18n, SerialTransport transport, List<string> invalidItems) :
            base(i18n, transport, invalidItems)
        {
            Initializer.Initialize(i18n);
            TranslatableLabel portLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Serial Port");
            portLabel.Xalign = 0;
            string[] availableSerialPorts = null;
            try {
                availableSerialPorts = SerialPort.GetPortNames();
            } catch (Exception) {
                Logging.Logger.Warning("List of serial ports could not be queried.");
            }
            if (availableSerialPorts == null) {
                availableSerialPorts = new string[0];
            }
            portComboBoxEntry = new Gtk.ComboBoxEntry(availableSerialPorts);
            portComboBoxEntry.Changed += (o, args) => Check();
            string genericTransportName = GenericTransport.PortName;
            if (availableSerialPorts.Length == 0) {
                portComboBoxEntry.Entry.Text = String.Empty;
            } else if (availableSerialPorts.AsEnumerable().Contains(genericTransportName)) {
                portComboBoxEntry.SetActiveIter(GetIterFromItem(genericTransportName).Value);
            } else {
                string firstCom = availableSerialPorts.First(p => p.StartsWith("COM"));
                if (firstCom != null) {
                    portComboBoxEntry.SetActiveIter(GetIterFromItem(firstCom).Value);
                } else {
                    portComboBoxEntry.SetActiveIter(GetIterFromItem(availableSerialPorts[0]).Value);
                }
            }
            AddConfigurationRow(portLabel, portComboBoxEntry);

            TranslatableLabel baudRateLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Baud Rate");
            baudRateLabel.Xalign = 0;

            baudRateComboBox = new Gtk.ComboBox(baudRateNames.ToArray());
            baudRateComboBox.Active = GetActiveSpeedId(GenericTransport.BaudRate);

            AddConfigurationRow(baudRateLabel, baudRateComboBox);
        }

        private Nullable<Gtk.TreeIter> GetIterFromItem(string lookingFor)
        {
            Gtk.TreeModel model = portComboBoxEntry.Model;
            for (int i = 0; i < model.IterNChildren(); i++) {
                Gtk.TreeIter iter;
                model.IterNthChild(out iter, i);
                string item = (string)model.GetValue(iter, 0);
                if (item == lookingFor) {
                    return iter;
                }
            }
            return null;
        }

        /// <inheritdoc/>
        public override void Save()
        {
            GenericTransport.PortName = portComboBoxEntry.Entry.Text;
            GenericTransport.BaudRate = GetSelectedBaudRate();
        }

        /// <inheritdoc/>
        public override void Check()
        {
            string selectedPort = portComboBoxEntry.ActiveText;
            if (InvalidItems.Contains(selectedPort)) {
                NotifyDisplayAlertRequest(
                    i18n.TrObject(Constants.UiDomain, "Serial port '{0}' already used.", selectedPort), null);
            } else {
                NotifyHideAlertRequest(null);
            }
        }

        private int GetActiveSpeedId(int baudRate)
        {
            string baudRateName = String.Format(CultureInfo.InvariantCulture, "{0}", baudRate);
            return baudRateNames.IndexOf(baudRateName);
        }

        private int GetSelectedBaudRate()
        {
            string selectedString = baudRateComboBox.ActiveText;
            int result = int.Parse(selectedString, CultureInfo.InvariantCulture);
            return result;
        }
    }
}

