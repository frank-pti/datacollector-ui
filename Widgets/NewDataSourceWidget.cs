using Datacollector.Ui.Model;
using Internationalization;
using InternationalizationUi;
using Logging;
using ProbeNet.Backend;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// New data source widget.
    /// </summary>
    public class NewDataSourceWidget : Gtk.VBox
    {
        /// <summary>
        /// Delegate method for backend events.
        /// </summary>
        public delegate void BackendDelegate(DeviceInformation selectedBackend);
        /// <summary>
        /// Occurs when backend activated.
        /// </summary>
        public event BackendDelegate BackendActivated;
        /// <summary>
        /// Occurs when backend selection changed.
        /// </summary>
        public event BackendDelegate BackendSelectionChanged;

        private I18n i18n;
        private IDeviceInformationStore deviceStore;
        private Gtk.Entry filterTextEntry;
        private DeviceInformationFilter treeFilter;
        private Gtk.TreeView manufacturerTree;
        private Gtk.TreeView tree;
        private string programText;
        private ManufacturerStore manufacturerModel;
        private IList<string> licensedIdentifiers;
        private TranslatableCheckButton listLicensedOnlyCheckButton;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.NewDataSourceWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="deviceStore">Device store.</param>
        /// <param name="manufacturerModel">Manufacturer model.</param>
        public NewDataSourceWidget(
            I18n i18n, IDeviceInformationStore deviceStore, ManufacturerStore manufacturerModel, string programText, IList<string> licensedIdentifiers) :
            base()
        {
            this.i18n = i18n;
            this.deviceStore = deviceStore;
            this.programText = programText;
            this.licensedIdentifiers = licensedIdentifiers;
            i18n.Changed += HandleI18nChanged;

            TranslatableLabel filterTextLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Search:") {
                Xalign = 0f
            };
            filterTextEntry = new Gtk.Entry() {
                WidthChars = 40
            };
            filterTextEntry.Changed += HandleFilterWidgetChanged;
            TranslatableIconButton printDeviceListButton = new TranslatableIconButton(
                i18n, Constants.UiDomain, "Print list", Gtk.Stock.Print);
            printDeviceListButton.Clicked += PrintDeviceListButtonClicked;

            listLicensedOnlyCheckButton = new TranslatableCheckButton(i18n, Constants.UiDomain, "List licensed data sources only") {
                Sensitive = licensedIdentifiers != null && licensedIdentifiers.Count > 0
            };
            listLicensedOnlyCheckButton.Clicked += ListLicensedOnlyCheckButtonClicked;

            uint x = 0, y = 0;
            Gtk.Table filterTable = new Gtk.Table(2, 2, false);
            filterTable.Attach(filterTextLabel, x, x + 1, y, y + 1, Gtk.AttachOptions.Fill, Gtk.AttachOptions.Fill, 5, 5);
            x++;
            filterTable.Attach(filterTextEntry, x, x + 1, y, y + 1, Gtk.AttachOptions.Fill, Gtk.AttachOptions.Fill, 5, 5);
            x++;
            filterTable.Attach(new Gtk.Label(""), x, x + 1, y, y + 1, Gtk.AttachOptions.Expand | Gtk.AttachOptions.Fill, Gtk.AttachOptions.Fill, 5, 5);
            x++;
            filterTable.Attach(printDeviceListButton, x, x + 1, y, y + 1, Gtk.AttachOptions.Fill, Gtk.AttachOptions.Fill, 5, 5);
            y++;
            x = 0;
            filterTable.Attach(listLicensedOnlyCheckButton, x, x + 2, y, y + 1, Gtk.AttachOptions.Fill, Gtk.AttachOptions.Fill, 5, 0);
            y++;
            x = 0;
            PackStart(filterTable, false, true, 0);
            Gtk.HPaned hpaned = new Gtk.HPaned() {
                Position = 132
            };

            this.manufacturerModel = manufacturerModel;
            manufacturerTree = new ManufacturerListView(i18n, manufacturerModel);
            manufacturerTree.Selection.Changed += HandleFilterWidgetChanged;

            Gtk.ScrolledWindow manufacturerScroller = new Gtk.ScrolledWindow();
            manufacturerScroller.Add(manufacturerTree);
            hpaned.Add1(manufacturerScroller);

            treeFilter = new DeviceInformationFilter(deviceStore, null);
            treeFilter.VisibleFunc = new DeviceInformationFilter.DeviceVisibleFunc(FilterTree);

            tree = new DataSourceListView(i18n, treeFilter);
            tree.RowActivated += HandleTreeRowActivated;
            tree.Selection.Changed += HandleSelectionChanged;

            Gtk.ScrolledWindow scroller = new Gtk.ScrolledWindow();
            scroller.Add(tree);

            hpaned.Add2(scroller);

            PackStart(hpaned, true, true, 5);
        }

        private void ListLicensedOnlyCheckButtonClicked(object sender, EventArgs e)
        {
            UpdateTreeFilter();
        }

        private void PrintDeviceListButtonClicked(object sender, EventArgs e)
        {
            new DeviceListPrinter(i18n, deviceStore, programText).Print();
        }

        private void HandleSelectionChanged(object sender, EventArgs e)
        {
            if (BackendSelectionChanged != null) {
                BackendSelectionChanged(SelectedBackend);
            }
        }

        private void HandleTreeRowActivated(object o, Gtk.RowActivatedArgs args)
        {
            if (BackendActivated != null) {
                BackendActivated(SelectedBackend);
            }
        }

        private void HandleFilterWidgetChanged(object sender, EventArgs e)
        {
            UpdateTreeFilter();
        }

        private void UpdateTreeFilter()
        {
            treeFilter.Refilter();
        }

        private bool FilterTree(IDeviceInformationStore model, Gtk.TreeIter iter)
        {
            try {
                DeviceInformation deviceInformation = model.GetValue(iter, 0) as DeviceInformation;
                ManufacturerInformation activeManufacturer = null;
                Gtk.TreeSelection manufacturerSelection = manufacturerTree.Selection;
                if (manufacturerSelection != null) {
                    Gtk.TreeIter selectedManuIter;
                    manufacturerSelection.GetSelected(out selectedManuIter);
                    if (manufacturerModel.IterIsValid(selectedManuIter)) {
                        activeManufacturer = manufacturerModel.GetValue(selectedManuIter, 0) as ManufacturerInformation;
                    }
                }
                if (activeManufacturer == null || deviceInformation == null) {
                    return CheckLicensedOnlyFilter(deviceInformation);
                }
                if (activeManufacturer.IsCatchAll) {
                    return deviceInformation.Manufacturer == null;
                }
                if (deviceInformation.Manufacturer == null) {
                    return false;
                }

                string text = model.GetValue(iter, 1).ToString().ToLowerInvariant();
                string filterText = filterTextEntry.Text.Trim().ToLowerInvariant();

                if (activeManufacturer.ContainsAll || activeManufacturer == deviceInformation.Manufacturer) {
                    if (String.IsNullOrWhiteSpace(filterText) || text.Contains(filterText)) {
                        return CheckLicensedOnlyFilter(deviceInformation);
                    }
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
            return false;
        }

        private bool CheckLicensedOnlyFilter(DeviceInformation device)
        {
            if (!listLicensedOnlyCheckButton.Active || licensedIdentifiers == null) {
                return true;
            }
            return licensedIdentifiers.Contains(device.Identifier);
        }

        private void HandleI18nChanged(I18n source, string domain)
        {
            Gtk.TreeIter iter;
            deviceStore.GetIterFirst(out iter);
            do {
                DeviceInformation deviceInformation = deviceStore.GetValue(iter, 0) as DeviceInformation;
                string name = deviceInformation.Name.Tr;
                deviceStore.SetValue(iter, 1, name);
            } while (deviceStore.IterNext(ref iter));
        }

        /// <summary>
        /// Gets the selected backend.
        /// </summary>
        /// <value>The selected backend.</value>
        public DeviceInformation SelectedBackend
        {
            get
            {
                Gtk.TreeSelection selection = tree.Selection;
                if (selection.CountSelectedRows() == 0) {
                    return null;
                }
                Gtk.TreeIter iter;
                selection.GetSelected(out iter);
                DeviceInformation information = tree.Model.GetValue(iter, 0) as DeviceInformation;
                return information;
            }
        }
    }
}

