/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// List view for data sources.
    /// </summary>
    public class DataSourceListView: Gtk.TreeView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.DataSourceListView"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="model">Model.</param>
        public DataSourceListView (I18n i18n, Gtk.TreeModel model):
            base(model)
        {
            Initializer.Initialize(i18n);
            Gtk.TreeViewColumn nameColumn = new Gtk.TreeViewColumn();
            nameColumn.Widget = new TranslatableLabel(i18n, Constants.UiDomain, "Data Source");
            nameColumn.Widget.ShowAll();
            
            Gtk.CellRendererPixbuf iconCell = new Gtk.CellRendererPixbuf();
            nameColumn.PackStart(iconCell, false);
            nameColumn.AddAttribute(iconCell, "pixbuf", 2);
            
            Gtk.CellRendererText nameCell = new Gtk.CellRendererText();
            nameColumn.PackStart(nameCell, true);
            nameColumn.AddAttribute(nameCell, "text", 1);
            
            AppendColumn(nameColumn);
            Selection.Mode = Gtk.SelectionMode.Single;
            Shown += HandleTreeShown;
        }

        private void HandleTreeShown (object sender, EventArgs e)
        {
            Gtk.TreeIter iter;
            Model.GetIterFirst(out iter);
            if (Model is Gtk.ListStore && ((Model as Gtk.ListStore).IterIsValid(iter))) {
                Selection.SelectIter(iter);
            }
        }
    }
}

