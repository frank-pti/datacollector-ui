/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Datacollector.Sources;
using System.Timers;
using Datacollector.Configuration;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// Widget showing incoming or outgoing data activity.
    /// </summary>
    public class DataSourceActivityWidget<D>: Gtk.VBox
        where D: DataSourceSettings
    {
        private Timer incomingTimer;
        private Timer outgoingTimer;

        private Gtk.Widget incomingActiveWidget;
        private Gtk.Widget incomingInactiveWidget;
        private Gtk.Widget outgoingActiveWidget;
        private Gtk.Widget outgoingInactiveWidget;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.DataSourceActivityWidget`1"/> class.
        /// </summary>
        /// <param name="dataSource">Data source.</param>
        /// <param name="incomingActiveWidget">Incoming active widget.</param>
        /// <param name="incomingInactiveWidget">Incoming inactive widget.</param>
        /// <param name="outgoingActiveWidget">Outgoing active widget.</param>
        /// <param name="outgoingInactiveWidget">Outgoing inactive widget.</param>
        public DataSourceActivityWidget(
            DataSource<D> dataSource,
            Gtk.Widget incomingActiveWidget,
            Gtk.Widget incomingInactiveWidget,
            Gtk.Widget outgoingActiveWidget,
            Gtk.Widget outgoingInactiveWidget
            )
        {
            incomingTimer = new Timer(200);
            outgoingTimer = new Timer(200);
            incomingTimer.Elapsed += HandleIncomingTimerElapsed;
            outgoingTimer.Elapsed += HandleOutgoingTimerElapsed;
            this.incomingActiveWidget = incomingActiveWidget;
            this.incomingInactiveWidget = incomingInactiveWidget;
            this.outgoingActiveWidget = outgoingActiveWidget;
            this.outgoingInactiveWidget = outgoingInactiveWidget;
            incomingActiveWidget.NoShowAll = true;
            incomingInactiveWidget.NoShowAll = true;
            outgoingActiveWidget.NoShowAll = true;
            outgoingInactiveWidget.NoShowAll = true;

            dataSource.IncomingTransferActive += HandleIncomingTransferActive;
            dataSource.OutgoingTransferActive += HandleOutgoingTransferActive;
            dataSource.ActiveStateChanged += HandleActiveStateChanged;

            PackStart(outgoingActiveWidget, false, false, 2);
            PackStart(outgoingInactiveWidget, false, false, 2);
            PackStart(incomingActiveWidget, false, false, 2);
            PackStart(incomingInactiveWidget, false, false, 2);

            IncomingActive = false;
            OutgoingActive = false;
        }

        private void HandleActiveStateChanged (DataSource<D> dataSource, Datacollector.Transportation.ConnectionState state)
        {
            switch(state) {
            case Datacollector.Transportation.ConnectionState.Connected:
                break;
            default:
                Gtk.Application.Invoke(delegate {
                    OutgoingActive = false;
                    IncomingActive = false;
                });
                break;
            }
        }

        private void HandleOutgoingTimerElapsed (object sender, ElapsedEventArgs e)
        {
            Gtk.Application.Invoke(delegate {
                OutgoingActive = false;
            });
            outgoingTimer.Enabled = false;
        }

        private void HandleIncomingTimerElapsed (object sender, ElapsedEventArgs e)
        {
            Gtk.Application.Invoke(delegate {
                IncomingActive = false;
            });
            incomingTimer.Enabled = false;
        }

        private void HandleOutgoingTransferActive (bool active)
        {
            if (active) {
                Gtk.Application.Invoke(delegate {
                    OutgoingActive = active;
                });
                outgoingTimer.Enabled = true;
            }
        }

        private void HandleIncomingTransferActive (bool active)
        {
            if (active) {
                Gtk.Application.Invoke(delegate {
                    IncomingActive = active;
                });
                incomingTimer.Enabled = true;
            }
        }

        private bool IncomingActive {
            get {
                return incomingActiveWidget.Visible;
            }
            set {
                incomingActiveWidget.Visible = value;
                incomingInactiveWidget.Visible = !value;
            }
        }

        private bool OutgoingActive {
            get {
                return outgoingActiveWidget.Visible;
            }
            set {
                outgoingActiveWidget.Visible = value;
                outgoingInactiveWidget.Visible = !value;
            }
        }
    }
}

