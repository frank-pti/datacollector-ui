using Datacollector.Ui.Model;
using Frank.Widgets.Model;
using Gtk;
using Internationalization;
using InternationalizationUi;
using ProbeNet.Messages.Raw;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Widgets
{
    /// <summary>
    /// List view for ProbeNet data sources.
    /// </summary>
    public class ProbeNetDataSourceListView : Gtk.TreeView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Widgets.ProbeNetDataSourceListView"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="model">Model.</param>
        public ProbeNetDataSourceListView(I18n i18n, DeviceDescriptionStore model) :
            base(model)
        {
            Initializer.Initialize(i18n);

            Gtk.TreeViewColumn nameColumn = new Gtk.TreeViewColumn();
            nameColumn.Widget = new TranslatableLabel(i18n, Constants.UiDomain, "Data Source");
            nameColumn.Widget.ShowAll();

            Gtk.TreeViewColumn serialNumberColumn = new Gtk.TreeViewColumn();
            serialNumberColumn.Widget = new TranslatableLabel(i18n, Constants.UiDomain, "Serial number");
            serialNumberColumn.Widget.ShowAll();

            Gtk.CellRendererPixbuf iconCell = new Gtk.CellRendererPixbuf();
            nameColumn.PackStart(iconCell, false);
            nameColumn.AddAttribute(iconCell, "pixbuf", 6);
            nameColumn.SetCellDataFunc(iconCell, new CellLayoutDataFunc(RenderSensitivity));

            Gtk.CellRendererText nameCell = new Gtk.CellRendererText();
            nameColumn.PackStart(nameCell, true);
            nameColumn.AddAttribute(nameCell, "text", 2);
            nameColumn.SetCellDataFunc(nameCell, new CellLayoutDataFunc(RenderSensitivity));

            Gtk.CellRendererText serialNumberCell = new Gtk.CellRendererText();
            serialNumberColumn.PackStart(serialNumberCell, true);
            serialNumberColumn.AddAttribute(serialNumberCell, "text", 3);
            serialNumberColumn.SetCellDataFunc(serialNumberCell, new CellLayoutDataFunc(RenderSensitivity));

            Gtk.CellRendererPixbuf warningConfiguredIconCell = new Gtk.CellRendererPixbuf() {
                IconName = "gtk-caps-lock-warning"
            };
            serialNumberColumn.PackStart(warningConfiguredIconCell, false);
            serialNumberColumn.SetCellDataFunc(warningConfiguredIconCell, new CellLayoutDataFunc(RenderConfiguredWarning));

            Gtk.CellRendererText warningConfiguredTextCell = new Gtk.CellRendererText() {
                Markup = String.Format("<i>{0}</i>", model.WarningConfiguredText.Tr)
            };
            serialNumberColumn.PackStart(warningConfiguredTextCell, false);
            serialNumberColumn.SetCellDataFunc(warningConfiguredTextCell, new CellLayoutDataFunc(RenderConfiguredWarning));

            AppendColumn(nameColumn);
            AppendColumn(serialNumberColumn);
            Selection.Mode = Gtk.SelectionMode.Single;
            Selection.SelectFunction = TreeSelectionFunc;
            Shown += HandleTreeShown;
        }

        private void RenderConfiguredWarning(CellLayout cellLayout, CellRenderer cell, TreeModel model, TreeIter iter)
        {
            TranslationString text = model.GetValue(iter, 8) as TranslationString;
            if (cell is CellRendererText) {
                (cell as CellRendererText).Text = text != null ? text.Tr : String.Empty;
            }
            cell.Visible = text != null && !String.IsNullOrEmpty(text.Tr);
        }

        private void RenderSensitivity(CellLayout cellLayout, CellRenderer cell, TreeModel model, TreeIter iter)
        {
            cell.Sensitive = (bool)model.GetValue(iter, 7);
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>The model.</value>
        public new DeviceDescriptionStore Model
        {
            get
            {
                return base.Model as DeviceDescriptionStore;
            }
        }

        private void HandleTreeShown(object sender, EventArgs e)
        {
            Gtk.TreeIter iter;
            Model.GetIterFirst(out iter);
            if (Model is Gtk.ListStore && ((Model as Gtk.ListStore).IterIsValid(iter))) {
                Selection.SelectIter(iter);
            }
        }

        private bool TreeSelectionFunc(TreeSelection selection, TreeModel model, TreePath path, bool pathCurrentlySelected)
        {
            TreeIter iter;
            model.GetIter(out iter, path);
            return (bool)model.GetValue(iter, 7);
        }

        private GenericListRow<Guid, Dictionary<string, DeviceDescription>, string, string, string, int, Gdk.Pixbuf, bool, TranslationString> SelectedRow
        {
            get
            {
                Gtk.TreeIter iter;
                Selection.GetSelected(out iter);
                if (!Model.IterIsValid(iter)) {
                    return null;
                }
                return Model[iter];
            }
        }

        /// <summary>
        /// Gets the selected device.
        /// </summary>
        /// <value>The selected device.</value>
        public Dictionary<string, DeviceDescription> SelectedDevice
        {
            get
            {
                if (SelectedRow != null) {
                    return SelectedRow.Item2;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the selected address.
        /// </summary>
        /// <value>The selected address.</value>
        public string SelectedAddress
        {
            get
            {
                if (SelectedRow != null) {
                    return SelectedRow.Item5;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the selected port.
        /// </summary>
        /// <value>The selected port.</value>
        public int SelectedPort
        {
            get
            {
                if (SelectedRow != null) {
                    return SelectedRow.Item6;
                }
                return 0;
            }
        }
    }
}

