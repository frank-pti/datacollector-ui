using Datacollector.Ui.Model;
using Datacollector.Ui.Widgets;
using Frank.Widgets.Dialogs;
using Internationalization;
using InternationalizationUi;
using InternationalizationUi.Mnemonic;
using ProbeNet.Backend;
using ProbeNet.Messages.Raw;
/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Datacollector.Ui.Dialogs
{
    /// <summary>
    /// Dialog providing a list of new data sources.
    /// </summary>
    public class NewDataSourceDialog : CustomDialog
    {
        private Gtk.Button addButton;
        private NewDataSourceWidget newDataSourceWidget;
        private NewProbeNetDataSourceWidget newProbeNetDataSourceWidget;
        private NewProbeNetDigitalDataSourceWidget newProbeNetDigitalDataSourceWidget;
        private Gtk.Notebook notebook;
        private DataSourceType selectedDataSourceType;

        /// <summary>Enumeration of data source types</summary>
        public enum DataSourceType : uint
        {
            /// <summary>
            /// The traditional data sources.
            /// </summary>
            Traditional = 0,
            /// <summary>
            /// ProbeNet digital data sources 
            /// </summary>
            /// <remarks>
            /// ProbeNet digital data sources are simple devices that are usually run be a micro controller only.
            /// </remarks>
            ProbeNetDigital = 1,
            /// <summary>
            /// ProbeNet data sources.
            /// </summary>
            /// <remarks>
            /// ProbeNet data sources are complex devices that are run by an internal PC.
            /// </remarks>
            ProbeNet = 2
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Dialogs.NewDataSourceDialog"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="deviceStore">Device store.</param>
        /// <param name="manufacturerStore">Manufacturer store.</param>
        public NewDataSourceDialog(
            I18n i18n, Gtk.Window parent, IDeviceInformationStore deviceStore, ManufacturerStore manufacturerStore,
            string programText, IList<string> licensedIdentifiers, IEnumerable<Guid> configuredDataSources,
            DeviceDescriptionStore cached) :
            base(i18n, i18n.TrObject(Constants.UiDomain, "Add new datasource"), parent, Gtk.DialogFlags.Modal,
       Gtk.ButtonsType.Close)
        {
            notebook = new Gtk.Notebook();
            if (cached != null) {
                cached.Initialize(i18n);
            }

            newDataSourceWidget = new NewDataSourceWidget(
                i18n, deviceStore, manufacturerStore, programText, licensedIdentifiers);
            newDataSourceWidget.BackendActivated += HandleBackendActivated;
            newDataSourceWidget.BackendSelectionChanged += HandleBackendSelectionChanged;

            TranslatableLabel newDataSourceLabel = new TranslatableLabel(i18n, Constants.UiDomain, "Traditional datasource");
            notebook.AppendPage(newDataSourceWidget, newDataSourceLabel);
            VBox.PackStart(notebook, true, true, 0);

            TranslatableLabel newProbeNetDigitalDataSourceLabel = new TranslatableLabel(i18n, Constants.UiDomain, "ProbeNet Digital datasource");
            newProbeNetDigitalDataSourceWidget = new NewProbeNetDigitalDataSourceWidget(i18n, configuredDataSources, cached);
            newProbeNetDigitalDataSourceWidget.DeviceActivated += HandleDeviceActivated;
            newProbeNetDigitalDataSourceWidget.DeviceSelectionChanged += HandleDeviceSelectionChanged;
            notebook.AppendPage(newProbeNetDigitalDataSourceWidget, newProbeNetDigitalDataSourceLabel);

            TranslatableLabel newProbeNetDataSourceLabel = new TranslatableLabel(i18n, Constants.UiDomain, "ProbeNet datasource");
            newProbeNetDataSourceWidget = new NewProbeNetDataSourceWidget(i18n, configuredDataSources);
            if (newProbeNetDataSourceWidget.HasValidConnection) {
                newProbeNetDataSourceWidget.DeviceActivated += HandleDeviceActivated;
                newProbeNetDataSourceWidget.DeviceSelectionChanged += HandleDeviceSelectionChanged;
                notebook.AppendPage(newProbeNetDataSourceWidget, newProbeNetDataSourceLabel);
            }

            addButton = new TranslatableMnemonicIconButton(i18n, Constants.UiDomain, "A_dd",
                new Gtk.Image(Gtk.Stock.Add, Gtk.IconSize.Button));
            addButton.Sensitive = false;
            addButton.Clicked += HandleAddButtonClicked;
            ActionArea.Add(addButton);
            ActionArea.ReorderChild(addButton, 0);
            SetSizeRequest(600, 400);
            notebook.SwitchPage += HandleSwitchPage;
            SelectedDataSourceType = DataSourceType.Traditional;
            ShowAll();
        }

        private void HandleDeviceSelectionChanged(Dictionary<string, DeviceDescription> selectedDevice)
        {
            addButton.Sensitive = selectedDevice != null;
        }

        private void HandleDeviceActivated(Dictionary<string, DeviceDescription> selectedDevice)
        {
            Respond(Gtk.ResponseType.Accept);
        }

        private void HandleSwitchPage(object o, Gtk.SwitchPageArgs args)
        {
            SelectedDataSourceType = (DataSourceType)args.PageNum;
        }

        /// <summary>
        /// Gets the type of the selected data source.
        /// </summary>
        /// <value>The type of the selected data source.</value>
        public DataSourceType SelectedDataSourceType
        {
            get
            {
                return selectedDataSourceType;
            }
            private set
            {
                selectedDataSourceType = value;
                if (selectedDataSourceType == DataSourceType.Traditional) {
                    addButton.Sensitive = newDataSourceWidget.SelectedBackend != null;
                } else if (selectedDataSourceType == DataSourceType.ProbeNetDigital) {
                    addButton.Sensitive = newProbeNetDigitalDataSourceWidget.SelectedDevice != null;
                } else {
                    addButton.Sensitive = newProbeNetDataSourceWidget.SelectedDevice != null;
                }
            }
        }

        private void HandleBackendActivated(DeviceInformation backend)
        {
            Respond(Gtk.ResponseType.Accept);
        }

        private void HandleBackendSelectionChanged(DeviceInformation backend)
        {
            addButton.Sensitive = backend != null;
        }

        /// <summary>
        /// Gets the selected traditional backend.
        /// </summary>
        /// <value>The selected traditional backend.</value>
        public DeviceInformation SelectedTraditionalBackend
        {
            get
            {
                return newDataSourceWidget.SelectedBackend;
            }
        }

        /// <summary>
        /// Gets the selected probe net device.
        /// </summary>
        /// <value>The selected probe net device.</value>
        public Dictionary<string, DeviceDescription> SelectedProbeNetDevice
        {
            get
            {
                switch (SelectedDataSourceType) {
                    case DataSourceType.ProbeNetDigital:
                        return newProbeNetDigitalDataSourceWidget.SelectedDevice;
                    case DataSourceType.ProbeNet:
                        return newProbeNetDataSourceWidget.SelectedDevice;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the selected probe net address.
        /// </summary>
        /// <value>The selected probe net address.</value>
        public string SelectedProbeNetAddress
        {
            get
            {
                return newProbeNetDataSourceWidget.SelectedAddress;
            }
        }

        /// <summary>
        /// Gets the selected probe net port.
        /// </summary>
        /// <value>The selected probe net port.</value>
        public int SelectedProbeNetPort
        {
            get
            {
                switch (SelectedDataSourceType) {
                    case DataSourceType.ProbeNetDigital:
                        return newProbeNetDigitalDataSourceWidget.SelectedPort;
                    case DataSourceType.ProbeNet:
                        return newProbeNetDataSourceWidget.SelectedPort;
                }
                return 0;
            }
        }

        private void HandleAddButtonClicked(object sender, EventArgs e)
        {
            Respond(Gtk.ResponseType.Accept);
        }

        /// <inheritdoc/>
        protected override void OnHidden()
        {
            notebook.SwitchPage -= HandleSwitchPage;
            base.OnHidden();
            newProbeNetDataSourceWidget.SearchEnabled = false;
            newProbeNetDigitalDataSourceWidget.DigitalDeviceFinder.Stop();
        }

        public DeviceDescriptionStore FoundDevices
        {
            get
            {
                return newProbeNetDigitalDataSourceWidget.FoundDevices;
            }
        }
    }
}
