/*
 * Gtk# widgets for the base library for collecting data from measurement devices
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Datacollector.Sources;
using InternationalizationUi;
using Frank.Widgets.Dialogs;
using Datacollector.Ui.Widgets.Configuration;
using Datacollector.Configuration;
using Gtk;
using System.Collections.Generic;

namespace Datacollector.Ui.Dialogs
{
    /// <summary>
    /// Data source configuration dialog.
    /// </summary>
    public class ConfigurationDialog<T> : CustomDialog
        where T : DataSourceSettings
    {
        private ConfigurationPage<T> page;
        private Button saveButton;
        private Button cancelButton;

        /// <summary>
        /// Initializes a new instance of the <see cref="Datacollector.Ui.Dialogs.ConfigurationDialog`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="dataSource">Data source.</param>
        /// <param name="dataSourceSettingsFrame">Data source settings frame.</param>
        /// <param name="invalidItems">List of invalid items.</param>
        /// <param name="dataPresent">If set to <c>true</c> data present.</param>
        public ConfigurationDialog(
            I18n i18n,
            Window parent,
            DataSource<T> dataSource,
            DataSourceSettingsFrame dataSourceSettingsFrame,
            List<string> invalidItems,
            bool dataPresent) :
            base(i18n, i18n.TrObject(Constants.UiDomain, "Configure"), parent)
        {
            saveButton = new TranslatableIconButton(
                i18n, Constants.UiDomain, "_Save", Gtk.Image.NewFromIconName(Gtk.Stock.Save, Gtk.IconSize.Button)) {
                    CanDefault = true
                };
            saveButton.Clicked += HandleButtonClicked;
            cancelButton = new TranslatableIconButton(
                i18n, Constants.UiDomain, "_Cancel", Gtk.Image.NewFromIconName(Gtk.Stock.Cancel, Gtk.IconSize.Button));
            cancelButton.Clicked += HandleButtonClicked;

            AddButton(saveButton, Gtk.ResponseType.Accept);
            AddButton(cancelButton, Gtk.ResponseType.Cancel);

            page = new ConfigurationPage<T>(i18n, this, dataSource, dataSourceSettingsFrame, invalidItems, dataPresent);
            page.DisplayAlertRequest += HandleDisplayAlertRequest;
            page.HideAlertRequest += HandleHideAlertRequest;

            VBox.PackStart(page);
            Default = saveButton;

            dataSourceSettingsFrame.SetConfirmActive += HandleSetConfirmActive;

            ShowAll();
        }

        private void HandleSetConfirmActive(bool active)
        {
            Gtk.Application.Invoke(delegate {
                saveButton.Sensitive = active;
            });
        }

        private void HandleButtonClicked(object sender, EventArgs args)
        {
            if (sender.Equals(saveButton)) {
                page.Save();
            }
        }

        private void HandleDisplayAlertRequest(TranslationString message, ResponseType? disableResponse)
        {
            DisplayAlert(message, disableResponse);
        }

        private void HandleHideAlertRequest(ResponseType? enableResponse)
        {
            HideAlert(enableResponse);
        }

        /// <summary>
        /// Gets the data source.
        /// </summary>
        /// <value>The data source.</value>
        public @DataSource<T> DataSource
        {
            get
            {
                return page.DataSource;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this dialog delete data on save.
        /// </summary>
        /// <value><c>true</c> if delete data on save; otherwise, <c>false</c>.</value>
        public bool DeleteDataOnSave
        {
            get
            {
                return page.DeleteDataOnSave;
            }
        }
    }
}

